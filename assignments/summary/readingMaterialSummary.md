### what is 'IoT'?
* The Internet of Things (IoT) refers to a system of interrelated, internet-connected objects  
  that are able to collect and transfer data over a wireless network without human intervention.
  
   In simple terms it is anything besides a computer,that is connected to the internet,has computational power
   and as it is connected to the internet it has resources that are not locally available.

   ![](https://www.cloudwards.net/wp-content/uploads/2018/06/IoT.png)	

![## IoT in various fields](https://lh3.googleusercontent.com/proxy/IlItMhQk2ggF9gpYR7DmxLXnaC0zcPXPQPFdKf_1JrcBtwKi2ELFam7TLhSe7-tmYDXSqGyfW5tHWfvdQPEEDUjpF5PDzMk3o01jGUF2qVd1zDUEP8LPQP8J-3pyBg)

## Industrial IoT
* The Industrial Internet of Things (IIoT), refers to interconnected sensors, instruments, and other devices
  networked together with computers' industrial applications, including manufacturing and energy management.
* The only difference between those two is their general usages. While IoT is most commonly used for consumer usage,
  IIoT is used for industrial purpose such as manufacturing, supply chain monitor and management system

 ## Industrial Revolution in Iot

* Industry 1.0 - Mechanization, Steam Power, Weaving loom
* Industry 2.0 - Mass production, Assembly lines, Electrical energy
* Industry 3.0 - Automation, Computers and Electronics,Information Technology 
* Industry 4.0 - Cyber Physical Systems, Internet of Things, Network 

 ![](https://pinaclsolutions.com/content/mixed-content/how-is-iot-helping-to-drive-the-4th-industrial-revolution/industrail-revolution.jpg)

 ## Industry 3.0

* Data is stored in databases and represented in excel sheets.
* IT and computer technology are used to automate processes
* Automation, Computers & Electronics.
* One of the major highlight of this revolution is the capablity of storing the acquried data from sensors in databases

### Typical Industry 3.0 Architecture
* Sensors --> PLC's --> SCADA & ERP
* Sensors  are installed at various points in the factory to send data to PLC's
  which inturn collects and sends it to SCADA storing the data.
* **Field Devices --> Control devices --> Station --> WorkCentres --> Enterprise**

  ![Interaction between hierarchies levels in Industry 3.0](https://dzone.com/storage/temp/7329932-hierarchy-copy.jpg)


* Field devices
   * sensors
   * acutators
   * Motors
    
* Control devices
   * Microcontrollers
   * Computer Numeric Controls

* Stations
* WorkCentres
    * Use softwares like scada,excel to store data 
* Enterprises

## Industry 3.0 Communication Protocols:-
* Protocols are the guidlines used to for sending and receiving data.
* All these protocols are optimized for sending data to a central server inside the factory.
* These protocols are used by sensors to Send data to PLC's. These protocols are called as _fieldbus_.

###Some of the protocols used by Industry 3.0 are
 * Modbus 
 * Profi-Network
 * CANopen
 * EtherCAT


## Industry 4.0
 Industry 4.0 is industry 3.0 devices connected to the internet (IOT) and when these devices is connected to internet they will send the data to the cloud.
 These data collected in the cloud gives us the advantage to have an overview of everything at a glance by giving us:-
  * Dashboards
  * Remote Web SCADA
  * Remote control configuration of devices.
  * Predictive maintenance.
  * Real-time event stream processing.
  * Analytics with predictive models.
  * Automated device provisioning,Auto discovery.
  * Real-time alerts & alarms.  

  ![](https://miro.medium.com/max/3762/1*FwH9u2aV-m8nEbnSB7ffGg.png)


## Industry 4.0 Communication Protocols

  Some Protocols used to send data to cloud for data analysis are- 
  * MQTT.org
  * AMQP
  * OPC UA 
  * CoAP RFC 7252
  * HTTP
  * WebSocket 
  * RESTful API

### Problems Faced with Industry 4.0 upgradation

  * Cost:-Industry 3.0 devices are very expensive
  * Downtime:-changing hardware means a big factory downtime.
  * Reliablity:-unproven and unreliable.

## Converting Indusry 3.0 devices to Industry 4.0:

To convert a 3.0 to 4.0, a library that helps, get data from industry 3.0 devices and send to industry 4.0 cloud.
  * Identify most popular industry 3.0 devices
  * Study protocols that these devices communicate
  * Get data from the industry 3.0 devices
  * Send the data to cloud for industry 4.0

After the data is sent to the cloud the following processes can take place

* Store data in _Time Shared Data database(TSDB)_ using tools like
    * Prometheus
    * InfluxDB
    * Things Board
    * Grafana

* View **All** the data in the dash board
    * Grafana 
    * Thingsboard

* IoT platforms analyse the data 
    * AWS IoT
    * Google IoT
    * Azure IoT
    * Thingsboard 
![](https://kellton-revamp-prod.s3.amazonaws.com/s3fs-public/inline-images/IoT%20Platforms-03_0.jpg)


* Gets Alerts using platforms like 
   * Zaiper
   * Twilio 
   * Integromat
   * IFTTT

  




